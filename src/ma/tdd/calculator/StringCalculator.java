package ma.tdd.calculator;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator implements Calculator{

	//StringCalculator(){}
	private static LinkedList<String> delimitersList = new LinkedList<String>();
	private static String extractWithRegex(String regex, String numbers, int g){		
		Pattern p = Pattern.compile(regex);
	    Matcher matcher = p.matcher(numbers);	    		
	    if (matcher.find()) {
	    	if(g==0)
	    		return matcher.group();
	    	else 
	    		return matcher.group(g);
	    }
		else return null;
	}
	private static LinkedList<String> extractDelimiters(String numbers){		
		String rawDelimiters = extractWithRegex("//(.*)\\n", numbers, 1);		
	    // use the reluctant quantifier, so to start the search from the beginning of the input string
		Pattern p = Pattern.compile("(\\[)(.*?)(\\])");		
		Matcher matcher = p.matcher(rawDelimiters);	
	    while(matcher.find()) {
	    	delimitersList.add(matcher.group(2));
	    }
	    return delimitersList;
	}
	private static String buildDelimiter(List<String> list){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int counter = 0;
		for (String s : list) {			
			sb.append(s);
			if (counter<list.size()-1) sb.append(",");
			counter++;
		}
		sb.append("]");
		return sb.toString();
	}
	
	public int Add(String numbers){
		String[] params = null;
		int sum = 0;
		String delimiter = "[,|\n]";
		String newNumbers = null;
		StringBuilder negativeNumbers = new StringBuilder();
		//try {
			if (numbers!=null) {				
				if (!numbers.isEmpty()){ 
					if (numbers.endsWith("\n"))
						throw new NumberFormatException("The numbers should'nt end with \\n");
					// to change a delimiter, the beginning of the string will contain a separate line that looks like this:   
					//  “//[delimiter]\n[numbers…]” for example “//;\n1;2”
					if (numbers.matches("//.?\n\\d.*"))
							{																
								delimiter = extractWithRegex("(//)(.{1})", numbers, 2);
								numbers = extractWithRegex("(//.?\\n{1})(.*)", numbers, 2);								
							}
					//Delimiters can be of any length with the following format:  “//[delimiter]\n” for example: “//[***]\n1***2***3” 
					else if (numbers.matches("//\\[.*\\]\n.*"))						
							{																												
							// Allow multiple delimiters like this:  “//[delim1][delim2]\n” for example “//[*][%]\n1*2%3” should return 6.
							delimiter = buildDelimiter(extractDelimiters(numbers));   //extractWithRegex("(\\[)(.*)(\\])", numbers, 2);									
							numbers = extractWithRegex("//(.*)\\n(.*)", numbers, 2);							
							}
//					String delimiter2 = "";
//					for (char c : delimiter.toCharArray()) {
//						if (String.valueOf(c).equals("*") || String.valueOf(c).equals("+") || String.valueOf(c).equals("+")
//								||String.valueOf(c).equals("?")) 
//							{delimiter2+="\\"+String.valueOf(c);}
//						else {delimiter2 +=String.valueOf(c);System.out.println("c!=\\*");}
//					}
//					System.out.println("delimiter2="+delimiter2);
//					delimiter = delimiter2;
										
					params = numbers.split(delimiter);
					int parsedint;
					for (String s : params) {
						// escape empty matches
						if (s.isEmpty()) { parsedint=0; continue;}
						parsedint =Integer.parseInt(s);						
						if (parsedint < 0) negativeNumbers.append(s).append(' ');
						//6- Ignore numbers > 1000
						if (parsedint > 1000) continue;
						sum +=parsedint;						
					}
					if (negativeNumbers.length()>0) {
						throw new NumberFormatException("negatives not allowed:"+negativeNumbers.toString());
					}
				}
			}	
//		} catch (NumberFormatException e) {
//			System.out.println("Exception :"+e.getMessage());
//		}
				
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringCalculator calc = new StringCalculator();
		//System.out.println(calc.extractWithRegex("(\\[)(.*)(\\])", "//[aaa]\n1aaa2aaa3",2));
		
		//System.out.println(calc.Add("//;\n1;2"));
		//System.out.println(calc.Add("//[***]\n1***2***3"));
		//System.out.println(calc.Add("//[???]\n1???4???5"));
		System.out.println(calc.Add("//[*][%]\n1*2%3"));
		System.out.println(calc.Add("//[****][%]\n1****2%3"));
		//System.out.println(calc.Add("//[aaa]\n1aaa2aaa4"));

	}

}
