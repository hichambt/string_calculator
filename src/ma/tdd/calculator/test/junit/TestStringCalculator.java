package ma.tdd.calculator.test.junit;

import static org.junit.Assert.assertEquals;
import ma.tdd.calculator.StringCalculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestStringCalculator {

	StringCalculator calc = new StringCalculator();
	@Test
	public final void testAdd() {		
		assertEquals(calc.Add(""), 0);
		assertEquals(calc.Add("1"), 1);
		assertEquals(calc.Add("1,2"), 3);
		assertEquals(calc.Add("1,2,3"), 6);
	}
	@Rule
	  public final ExpectedException exception = ExpectedException.none();
	//Allow the Add method to handle new lines between numbers (instead of commas).
	@Test
	public final void testAddNewLine() {				
		assertEquals(calc.Add("1\n2,3"), 6);
		//exception.expect(NumberFormatException.class);
		calc.Add("1,\n");
		assertEquals(calc.Add("//;\n1;2"), 3);
	}
	@Test	
	public final void testChangeDelimiter() {		
		System.out.println("------ third test-----");		
		assertEquals(calc.Add("//;\n1;2"), 3);
	}

	@Test
	public final void testNegativeNumbersNotAllowed() {						
		//exception.expect(NumberFormatException.class);		
		calc.Add("//;\n-11;2;-3;4");
	}

	@Test
	public final void testIgnoreLargeNumbers() {									
		assertEquals(calc.Add("//;\n1001;2"), 2);
	}
	@Test
	public final void testDelimiterOfanyLength() {											
		assertEquals(calc.Add("//[***]\n1***2***3"), 6);
		assertEquals(calc.Add("//[??]\n1??4??5"), 10);
		assertEquals(calc.Add("//[qqq]\n1qqq2qqq3"), 6);
	}

}
